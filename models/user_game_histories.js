'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User_game_histories.belongsTo(models.User_game, {
        foreignKey : "UserGameId",
        as : "userhistory",
      });
     
    }
  }
  User_game_histories.init({
    time_played: DataTypes.STRING,
    date: DataTypes.STRING,
    UserGameId : DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'User_game_histories',
  });
  return User_game_histories;
};