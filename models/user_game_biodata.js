'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game_biodata extends Model {
   
    static associate(models) {
      // define association here
      models.User_game_biodata.belongsTo(models.User_game, {
        foreignKey : "UserGameId",
        as : "biodata",
      }); 
    }
  }
  User_game_biodata.init({
    full_name: DataTypes.STRING,
    email: DataTypes.STRING,
    age: DataTypes.STRING,
    city: DataTypes.STRING,
    UserGameId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'User_game_biodata',
  });
  return User_game_biodata;
};