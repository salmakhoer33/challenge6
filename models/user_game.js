'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.User_game.hasMany(models.User_game_histories, { as: "histories" });
  
     

      models.User_game.hasOne(models.User_game_biodata, {
        foreignKey: "UserGameId",
        as : "biodata",
      });
    }
  }
  User_game.init({
    user_name: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_game',
  });
  return User_game;
};