const express = require("express");
const app = express();
const  PORT = 8000;
const { User_game,User_game_biodata,User_game_histories } = require('./models');
const user_game_biodata = require("./models/user_game_biodata");
const user_game_histories = require("./models/user_game_histories");
const data = require("./users.json");


app.use(express.json());
app.use(express.urlencoded({ extended : false}));
app.set ("view engine", "ejs");


const validation = (req, res, next) => {
    const { id } = req.body;
    const isFind = data.find((row) => row.id == id);
    if (isFind) {
        res.status (400).json({ message: "Id not available"});
    } else {
        next();
    }
};

app.post ('/user_gamess', (req,res) => {
    const {user_name, password} = req.body;
  
  User_game.create({
      user_name: user_name,
      password : password,
  
  }).then(user_game => {
      res.json({ message : "Create Success", data : user_game})
      })
  });

app.get ("/user_gamess", (req,res) => {
    User_game.findAll({ include: ["histories", "biodata"] }).then((user_game) => {
        res.json({message: "Fetch All success", data : user_game});
    });
});

app.get ("/user_gamess/:id", (req,res) => {
    const {id} = req.params;

    User_game.findOne({ include: ["histories", "biodata"], where :{ id : id}}).then((user_game) => {
        res.json({message: "Fetch success", data : user_game});
    });
});

app.put ("/user_gamess/:id", (req,res) => {
    const { id } = req.params;
    const {user_name, password} = req.body;

    User_game.update({user_name: user_name,password : password},{where: { id : id } })
    .then(() => {
        res.json({ message: "Updating Success"});
    })
    .catch((err) => {
        res.json({ message: "Failed Updating!"});
    });
});

app.delete("/user_gamess/:id", (req,res) => {
    const { id } = req.params;
    User_game.destroy({ include: ["histories", "biodata"], where: { id : id } })
    .then(() => {
        res.json({ message: "Delete Success"});
    })
    .catch((err) => {
        res.json({ message: "Failed to Delete!"});
    });
});

app.post ('/user_gamess_histories', (req,res) => {
    const { time_played,date,UserGameId } =req.body;
  
  User_game_histories.create({
   time_played:time_played,
   date:date,
   UserGameId : UserGameId,
      
  }).then(user_game_histories => {
      res.json({ message : "Create Success", data : user_game_histories})
      })
  });

  
  app.put ("/user_gamess_histories/:id", (req,res) => {
    const { id } = req.params;
    const {time_played,date,user_id} = req.body;

    User_game_histories.update({time_played:time_played,date:date,user_id:user_id},{where: { id : id } })
    .then(() => {
        res.json({ message: "Updating Success"});
    })
    .catch((err) => {
        res.json({ message: "Failed Updating!"});
    });
});


  app.get("/views/user_gamess/create", (req, res) => {
    res.render("user_gamess/form");
});

app.get("/views/user_gamess_biodata/create/biodata", (req, res) => {
    User_game_biodata.findAll().then((User_gamess) => {
        res.render("user_gamess/biodata",{User_gamess});
    });
});

app.get("/views/user_gamess_histories/create/history", (req, res) => {
    User_game_histories.findAll().then((User_gamess) => {
        res.render("user_gamess/histories",{User_gamess});
    });
});

app.get("/user_gamess_biodata", (req, res) => {
    const name = "user_name"
    res.render("form.ejs", {name});
});

app.get("/api/posts", (req, res) => {
    res.json(data);
  });

  app.post("user_gamess", (req, res) => {
    res.redirect("/user_gamess_biodata");
});

app.use ((req,res) => {
    res.status(404).json({ message : "its not found"});
});



app.listen(PORT, () => console.log(`Listening at http://localhost:${PORT}`));