'use strict';

const { promiseImpl } = require('ejs');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
   return Promise.all([
    queryInterface.addColumn("User_game_histories", "user_id", {
      type: Sequelize.INTEGER,
      allowNull :true,
    }),
   ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
